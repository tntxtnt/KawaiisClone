#ifndef GAME_H
#define GAME_H

#include <iostream>
#include "SFMLGrid.h"
#include "ResourceManager.h"
#include "SplashScreen.h"
#include "sftools/Chronometer.hpp"
#include "GameHUD.h"
#include "GameGUI.h"
#include "GameLevel.h"
#include "HighScore.h"

extern const std::string RES_PATH;
extern const std::string HISCR_PATH;
extern const int WINDOW_W;
extern const int WINDOW_H;
extern const int CELL_W;
extern const int CELL_H;
extern const int CELL_ROWS;
extern const int CELL_COLS;
extern const int GRID_POS_X;
extern const int GRID_POS_Y;
extern const int FPS;
extern const sf::Time MATCH_DELAY;
extern const sf::Time NEXT_LEVEL_DELAY;
extern const sf::Time SPLASH_TIME;
extern const sf::Time MATCH_TIME;
extern const sf::Time HINT_TIME;
extern const int MAX_LEVEL;
extern const int SCORE_PER_MATCH;
extern const int SCORE_PER_SHUFF;
extern const int SCORE_PER_HINT;
extern const int SCORE_PER_SEC;
extern const int INIT_HINT_COUNT;
extern const int INIT_SHUF_COUNT;
extern const int ADDED_HINT_COUNT;
extern const int ADDED_SHUF_COUNT;

const sf::Time UNPAUSE_DELAY = FADE_IN_DELAY + sf::milliseconds(50);

class Game
{
    friend class GameGUI;
// Singleton
public:
    static Game& instance()
    {
        static Game instance;
        return instance;
    }
private:
    Game();
public:
    Game(const Game&) = delete;
    Game& operator=(const Game&) = delete;
// END - Singleton
public:
    void run();
private:
    // Initialization
    bool mapResources();
    void setup();
    void newGame();
    // Game logic
    void handleFindPath();
    void handleHint();
    void handleGameTimer();
    void showHint(bool=false);
    void shuffle(bool=false);
    // Event handlers
    void handleKeyPressed(const sf::Event&);
    void handleMousePressed(const sf::Event&);
    void handleMouseMoved(const sf::Event&);
    // Rendering + play sound/music
    void renderGrid();
    void renderMenu();
    void playMusic();
    void playSound(sf::SoundBuffer&);
    // State handlers
    void handleMainMenuState();
    void handlePlayingState();
    void handlePausingState();
    void handleMatchDelayState();
    void handleNextLevelState();
    void handleGameOverState();
    void toGameOverState();
    void toMainMenuState(bool=true);
    void toMMFromPS();
    void handleGeneralInput(sf::Event&);
    // High scores
    bool loadHighScores();
    void saveHighScores()const;
private:
    sf::RenderWindow window;
    ResourceManager resMan;
    // Game resources
    sf::SoundBuffer wrongSoundBuf;
    sf::SoundBuffer correctSoundBuf;
    sf::SoundBuffer shuffleSoundBuf;
    sf::SoundBuffer clearedSoundBuf;
    sf::SoundBuffer gameOverSoundBuf;
    sf::Sound sound; //sound player
    sf::Music music;
    sf::Font font;
    // Game grid
    kawaii::SFMLGrid grid;
    // Game control variables
    int curLvId;
    int score;
    int shuffleCount;
    int hintCount;
    bool windowFocused;
    bool hintShown;
    sftools::Chronometer gameTimer;
    sftools::Chronometer timeSinceLastMatch;
    sf::Clock matchDelay;
    sf::Clock nextLevelDelay;
    sf::Clock unpauseDelay;
    kawaii::Coord hoverCoord;
    // HUD
    GameHUD hud;
    // Main loop handler
    std::function<void()> mainLoopCallback;
    // Level
    GameLevel level;
    // High scores, GUI
    std::vector<HighScore> highScores;
    sf::Texture hiScoreTex;
    GameGUI gui;
    // Background sprite
    sf::Sprite bgSprite;
};

#endif // GAME_H
