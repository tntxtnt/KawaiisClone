#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

//http://en.sfml-dev.org/forums/index.php?topic=4770.0
// This one is known: XLib.h defines a macro named "None",
//which conflicts with sf::Style::None.
// Basically, to avoid problems, you must make sure that
//you always include SFML headers first.

#include "ResourceManager.h"
#ifndef NO_SPLASH_SCREEN
#include "TransparentWindows.h"
#endif // NO_SPLASH_SCREEN

extern const sf::Time SPLASH_TIME;
extern const std::string RES_PATH;

class SplashScreen
{
public:
    static void run(sf::RenderWindow&, ResourceManager&);
};

#endif // SPLASHSCREEN_H
