#ifndef GAMEHUD_H
#define GAMEHUD_H

#include <SFML/Graphics.hpp>
#include "SFMLGrid.h"

extern const int WINDOW_W;
extern const int CELL_W;
extern const int CELL_H;
extern const int GRID_POS_X;
extern const int GRID_POS_Y;

//Display score, shuffleCount, hintCount, timerBar, hoverRect
struct GameHUD : public sf::Drawable, public sf::Transformable
{
    const kawaii::SFMLGrid& grid;
    const kawaii::Coord& hoverCoord;
    sf::Texture timerBarTex, hintTex, shuffleTex;
    sf::Sprite timerBarSprite, hintSprite, shuffleSprite;
    sf::Text scoreText, hintCountText, shuffleCountText;
    sf::RectangleShape timeElapsedBar, hoverRect;
    int scoreTextPosX;

    GameHUD(const kawaii::SFMLGrid& grid, const kawaii::Coord& hoverCoord)
    : grid(grid), hoverCoord(hoverCoord), scoreTextPosX(0) {}
    void setup();
    void updateScoreText(int);
    void updateHintCountText(int);
    void updateShuffleCountText(int);
    void syncHoverRectPosition();
    void draw(sf::RenderTarget&, sf::RenderStates)const;
};

#endif // GAMEHUD_H
