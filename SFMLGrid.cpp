#include "SFMLGrid.h"

namespace kawaii
{
SFMLGrid::SFMLGrid(int w, int h, int pairs)
: Grid(w, h, pairs), texR(0), texC(0),
  cellOutlineColor(155, 57, 108),
  cellFillColor(255, 224, 255),
  cellSelectedColor(191, 168, 191),
  cellHintColor(127, 255, 64)
{
}

bool SFMLGrid::setDrawingInfo(int cellW, int cellH, ResourceManager& man,
                              const std::string& texName,
                              int spriteW, int spriteH)
{
    if (!man.map(texture, texName)) return 0;
    this->spriteW = spriteW;
    this->spriteH = spriteH;
    this->cellW = cellW;
    this->cellH = cellH;
    texR = texture.getSize().y / spriteH;
    texC = texture.getSize().x / spriteW;
    return 1;
}

void SFMLGrid::setGridColors(const std::vector<sf::Color>& colors)
{
    cellOutlineColor = colors[0];
    cellFillColor = colors[1];
    cellSelectedColor = colors[2];
    cellHintColor = colors[3];
}

sf::IntRect SFMLGrid::getTexIntRect(int id)const
{
    int r = id / texC;
    int c = id % texC;
    return sf::IntRect(c*spriteW, r*spriteH, spriteW, spriteH);
}

void SFMLGrid::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    states.transform *= getTransform();
    drawGrid(target, states);
    drawPath(target, states);
}

void SFMLGrid::drawGrid(sf::RenderTarget& target, sf::RenderStates states)const
{
    sf::RectangleShape rect(sf::Vector2f(cellW-1, cellH-1));
    rect.setOutlineColor(cellOutlineColor);
    rect.setOutlineThickness(1);

    sf::Sprite sprite(texture);
    int spriteOffsetX = (cellW - 32)/2;
    int spriteOffsetY = (cellH - 32)/2;

    for (int r = 0; r < h; ++r, rect.move(-rect.getPosition().x, cellH))
        for (int c = 0; c < w; ++c, rect.move(cellW, 0))
            if (data[r][c])
            {
                // Set rect fill color
                if ((r == selections.first.r && c == selections.first.c)
                 || (r == selections.second.r && c == selections.second.c))
                    rect.setFillColor(cellSelectedColor);
                else if ((r == hint.first.r && c == hint.first.c)
                 ||      (r == hint.second.r && c == hint.second.c))
                    rect.setFillColor(cellHintColor);
                else
                    rect.setFillColor(cellFillColor);
                // Draw
                target.draw(rect, states);
                sprite.setTextureRect(getTexIntRect(data[r][c] - 1));
                sprite.setPosition(rect.getPosition().x + spriteOffsetX,
                                   rect.getPosition().y + spriteOffsetY);
                target.draw(sprite, states);
            }
}

void SFMLGrid::drawPath(sf::RenderTarget& target, sf::RenderStates states)const
{
    if (hasPath() && path.size() > 1)
    {
        sf::RectangleShape rect;
        rect.setFillColor(sf::Color::Red);
        for (size_t i = 1; i < path.size(); ++i)
        {
            sf::Vector2i p1 = sf::Vector2i(path[i].c, path[i].r);
            sf::Vector2i p2 = sf::Vector2i(path[i-1].c, path[i-1].r);
            if (p2.x < p1.x || p2.y < p1.y) std::swap(p1, p2);
            rect.setPosition(cellW/2 + p1.x*cellW, cellH/2 + p1.y*cellH);
            auto line = p2 - p1;
            if (line.x == 0) //vertical line
            {
                line *= cellH;
                line.x = 5;
                line.y += 5;
            }
            else
            {
                line *= cellW;
                line.x += 5;
                line.y = 5;
            }
            rect.setSize(sf::Vector2f(line.x, line.y));
            target.draw(rect, states);
        }
    }
}

Coord SFMLGrid::toGridIndex(int x, int y)const
{
    Coord p;
    p.r = y - getPosition().y;
    p.c = x - getPosition().x;
    if (p.r < 0 || p.c < 0) return Coord();
    p.r /= cellH;
    p.c /= cellW;
    if (p.r >= h || p.c >= w) return Coord();
    return p;
}

void SFMLGrid::handleMouseClick(int x, int y)
{
    auto clicked = toGridIndex(x, y);
    select(clicked.r, clicked.c);
}
}
