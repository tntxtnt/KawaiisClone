#include "Game.h"

Game::Game()
: grid(CELL_COLS, CELL_ROWS, 2), curLvId(0),
  score(0), shuffleCount(INIT_SHUF_COUNT), hintCount(INIT_HINT_COUNT),
  windowFocused(1), hintShown(0),
  hoverCoord(-1, -1), hud(grid, hoverCoord),
  level(grid, music, bgSprite, resMan)
{
    gui.setWindow(window);
}

void Game::newGame()
{
    // Map shift function to correct level
    level.sync(curLvId = 0);

    // Reset grid
    grid.reset(curLvId * 6);
    //std::cout << "Done grid.reset()\n";

    // Reset HUD
    hud.updateScoreText(score = 0);
    hud.updateShuffleCountText(shuffleCount = INIT_SHUF_COUNT);
    hud.updateHintCountText(hintCount = INIT_HINT_COUNT);

    hintShown = false;

    // Restart all timers' clocks
    gameTimer.reset(1);
    timeSinceLastMatch.reset(1);
    matchDelay.restart();
    nextLevelDelay.restart();

    // Reset hover coords
    hoverCoord.r = hoverCoord.c = -1;
    hud.syncHoverRectPosition();

    // Map main loop callback
    gui.hideMainMenuWidget();
    mainLoopCallback = std::bind(&Game::handlePlayingState, this);

    //std::cout << "Done newGame()\n";
}

void Game::setup()
{
    // Create new window
    window.create(sf::VideoMode(WINDOW_W, WINDOW_H), "Kawaii's Clone");
    window.setFramerateLimit(60);
    auto desktopRect = sf::VideoMode::getDesktopMode();
    window.setPosition(sf::Vector2i((desktopRect.width - WINDOW_W) / 2,
                                    (desktopRect.height - WINDOW_H) / 2 - 30));
    // Ready grid for drawing
    if (!grid.setDrawingInfo(CELL_W, CELL_H, resMan, "textures.png", 32, 32))
        exit(1);
    grid.setPosition(GRID_POS_X, GRID_POS_Y);

    // Music player
    music.setLoop(true);
    music.setVolume(20);
    // HUD
    hud.setup();
    hud.updateScoreText(score);
    hud.updateShuffleCountText(shuffleCount);
    hud.updateHintCountText(hintCount);
    // GUI
    gui.setWindow(window);
    gui.setFont(font);
    gui.loadMainMenuWidget();
    gui.loadHighScoreWidget();
    gui.loadPauseMenuWidget();
    // Main loop <== Main menu state
    toMainMenuState();
}

void Game::handleKeyPressed(const sf::Event& event)
{
    if (event.key.code == sf::Keyboard::F5)
        shuffle(true);
    else if (event.key.code == sf::Keyboard::F1)
        showHint(true);
    else if (event.key.code == sf::Keyboard::Escape)
    {   //pause
        unpauseDelay.restart();
        gui.showPauseMenuWidget();
        gameTimer.toggle();
        timeSinceLastMatch.toggle();
        mainLoopCallback = std::bind(&Game::handlePausingState, this);
    }
}

void Game::handleMousePressed(const sf::Event& event)
{
    if (event.mouseButton.button == sf::Mouse::Left)
        grid.handleMouseClick(event.mouseButton.x, event.mouseButton.y);
    else if (event.mouseButton.button == sf::Mouse::Right)
        grid.deselectLast();
}

void Game::handleMouseMoved(const sf::Event& event)
{
    hoverCoord = grid.toGridIndex(event.mouseMove.x, event.mouseMove.y);
    hud.syncHoverRectPosition();
}

void Game::handleFindPath()
{
    if (!grid.cleared() && grid.selected() == 2)
    {
        if (!grid.match())
        {
            playSound(wrongSoundBuf);
            grid.deselectLast();
        }
        else
        {
            grid.findPath();
            if (!grid.hasPath())
            {
                playSound(wrongSoundBuf);
                grid.deselectLast();
            }
            else //matched pair
            {
                playSound(correctSoundBuf);
                matchDelay.restart();
                mainLoopCallback = std::bind(&Game::handleMatchDelayState, this);
            }
        }
    }
}

void Game::showHint(bool force)
{
    if (hintShown) return;
    if (force)
    {
        if (!hintCount) return;
        hud.updateHintCountText(--hintCount);
    }
    grid.showHint();
    hintShown = true;
}

void Game::shuffle(bool forced)
{
    if (forced)
    {
        if (shuffleCount < 1) return;
        hud.updateShuffleCountText(--shuffleCount);
    }
    playSound(shuffleSoundBuf);
    grid.shuffle();
    grid.findRandomMatch();
    timeSinceLastMatch.reset(1);
    hintShown = false;
}

void Game::handleHint()
{
    if (!grid.cleared() && !hintShown
        && timeSinceLastMatch.getElapsedTime() > HINT_TIME)
            showHint();
}

void Game::handleGameTimer()
{
    if (!grid.cleared())
    {
        float elapsed = gameTimer.getElapsedTime().asSeconds();
        float percentage = elapsed / MATCH_TIME.asSeconds();
        if (percentage > 1.0f)
        {
            hud.timeElapsedBar.setSize(sf::Vector2f(hud.timerBarTex.getSize().x - 4, 15));
            playSound(gameOverSoundBuf);
            toGameOverState();
        }
        else
        {
            float elapsedW = (hud.timerBarTex.getSize().x - 4) * percentage;
            hud.timeElapsedBar.setSize(sf::Vector2f(elapsedW, 15));
        }
    }
}

void Game::renderGrid()
{
    window.clear();
    if (windowFocused)
    {
        window.draw(bgSprite); // Background image
        window.draw(grid);     // Grid
    }
    window.draw(hud); // HUD
    gui.draw();       // GUI
    window.display();
}

void Game::renderMenu()
{
    window.clear();
    gui.draw();
    window.display();
}

void Game::playSound(sf::SoundBuffer& buf)
{
    sound.setBuffer(buf);
    sound.play();
}

void Game::playMusic()
{
    if (windowFocused && music.getStatus()!=sf::Music::Playing)
        music.play();
    else if (!windowFocused && music.getStatus()==sf::Music::Playing)
        music.pause();
}

void Game::run()
{
    // Mount zipped resource pack
    if (!resMan.mount(RES_PATH)) exit(1);

    // SplashScreen::run will open the corresponding resource pack
    SplashScreen::run(window, resMan); //SplashScreen not work in Debug
    resMan.release("images", "splash.png");

    // Map resources
    if (!mapResources()) exit(1);

    // Setup srpites, resources, etc.
    setup();

    // Main loop
    while (window.isOpen())
        mainLoopCallback();
}

bool Game::mapResources()
{
    // Load sound effects
    if (!resMan.map(wrongSoundBuf, "wrong.wav")) return 0;
    if (!resMan.map(correctSoundBuf, "correct.wav")) return 0;
    if (!resMan.map(shuffleSoundBuf, "shuffle.wav")) return 0;
    if (!resMan.map(clearedSoundBuf, "cleared.wav")) return 0;
    if (!resMan.map(gameOverSoundBuf, "gameover.wav")) return 0;
    // `Open` music
    if (!resMan.map(music, "day27.ogg")) return 0;
    // Fonts
    if (!resMan.map(font, "FallingSkyLight.otf")) return 0;
    // Load timer bar texture & create sprite
    if (!resMan.map(hud.timerBarTex, "timer_bar.png")) return 0;
    if (!resMan.map(hud.shuffleTex, "389.gif")) return 0;
    if (!resMan.map(hud.hintTex, "392.gif")) return 0;
    if (!resMan.map(hiScoreTex, "hiscorebg.png")) return 0;
    // Load background images
    for (int i = 0; i < MAX_LEVEL; ++i)
    {
        std::string imgId = "BG" + std::to_string(i+1) + ".png";
        if (!resMan.map(level.bgTexs[i], imgId)) return 0;
    }
    bgSprite.setPosition(0, 0);

    // Map HUD fonts
    hud.scoreText.setFont(font);
    hud.shuffleCountText.setFont(font);
    hud.hintCountText.setFont(font);

    return 1;
}

// State handlers
void Game::handleGeneralInput(sf::Event& event)
{
    if (event.type == sf::Event::Closed)
        window.close();
    else if(event.type == sf::Event::GainedFocus)
        windowFocused = true;
    else if(event.type == sf::Event::LostFocus)
        windowFocused = false;
    gui.handleEvent(event);
}

void Game::handlePlayingState()
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        handleGeneralInput(event);
        if (!windowFocused) continue;
        if (event.type == sf::Event::KeyPressed)
            handleKeyPressed(event); //F1 F5 ESC
        else if (event.type == sf::Event::MouseMoved)
            handleMouseMoved(event); //hover
        else if (event.type == sf::Event::MouseButtonPressed)
            handleMousePressed(event); //choosing
    }
    handleFindPath();
    handleHint();
    handleGameTimer();
    renderGrid();
    playMusic();
}

void Game::handlePausingState()
{
    sf::Event event;
    while (window.pollEvent(event))
    {
        handleGeneralInput(event);
        if (event.type == sf::Event::KeyPressed
            && event.key.code == sf::Keyboard::Escape
            && gui.get<tgui::Grid>("PauseMenu")->isVisible()
            && unpauseDelay.getElapsedTime() >= UNPAUSE_DELAY)
          toMMFromPS();
    }
    renderMenu();
    if (music.getStatus() == sf::Music::Playing) music.pause();
}

void Game::handleMatchDelayState()
{
    //Don't handle any events

    if (matchDelay.getElapsedTime() >= MATCH_DELAY)
    {
        grid.removePair();
        grid.findRandomMatch();
        timeSinceLastMatch.reset(1);
        hintShown = false;
        hud.updateScoreText(score += SCORE_PER_MATCH);
        if (!grid.hasHint() && !grid.cleared()) //no more matched pair
        {
            if (!shuffleCount) //game over
            {
                playSound(gameOverSoundBuf);
                toGameOverState();
            }
            else //auto-shuffle, continue playing
            {
                shuffle(true);
                while (!grid.hasHint() && !grid.cleared())
                    shuffle(); //make sure there's a way
                mainLoopCallback = std::bind(&Game::handlePlayingState, this);
            }
        }
        else if (grid.cleared()) //next level or win
        {
            hud.updateScoreText(score += SCORE_PER_SHUFF*shuffleCount
                + SCORE_PER_HINT*hintCount
                + SCORE_PER_SEC*(MATCH_TIME - gameTimer.getElapsedTime()).asSeconds());
            if (curLvId+1 < MAX_LEVEL) //next level
            {
                playSound(clearedSoundBuf);
                nextLevelDelay.restart();
                mainLoopCallback = std::bind(&Game::handleNextLevelState, this);
            }
            else //win
            {
                playSound(clearedSoundBuf); //should be winSoundBuf
                toGameOverState();
            }
        }
        else //normal, continue playing
            mainLoopCallback = std::bind(&Game::handlePlayingState, this);
    }
    renderGrid();
    playMusic();
}

void Game::handleNextLevelState()
{
    //Just ignore all events (except close window of course)
    sf::Event event;
    while (window.pollEvent(event))
        handleGeneralInput(event);

    if (nextLevelDelay.getElapsedTime() >= NEXT_LEVEL_DELAY) //end of delay
    {
        // Change state
        mainLoopCallback = std::bind(&Game::handlePlayingState, this);
        // Reset hover coords
        hoverCoord.r = hoverCoord.c = -1;
        hud.syncHoverRectPosition();
        // Next level
        level.sync(++curLvId);
        // Change grid size to fit new level shift function
        if (curLvId >= 5)
        {
            int oldRows = grid.rows(), oldCols = grid.cols();
            grid.resize(oldCols/2*2, oldRows/2*2);
            grid.move(CELL_W / 2 * (oldCols - grid.cols()),
                      CELL_H / 2 * (oldRows - grid.rows()));
        }
        // Reset grid
        grid.reset(curLvId * 6); //6 == TEXTURE_COLS
        // Reset clocks, game timer, etc.
        timeSinceLastMatch.reset(1);
        matchDelay.restart();
        gameTimer.reset(1);
        hud.timeElapsedBar.setSize(sf::Vector2f(0, hud.timeElapsedBar.getSize().y));
        // Bonus shuffleCount and hintCount
        hud.updateShuffleCountText(shuffleCount += ADDED_SHUF_COUNT);
        hud.updateHintCountText(hintCount += ADDED_HINT_COUNT);
    }
    renderGrid();
    playMusic();
}

void Game::handleGameOverState()
{
    //input name event (if there is), confirm button clicked event
    sf::Event event;
    while (window.pollEvent(event))
        handleGeneralInput(event);
    renderGrid();
    playMusic();
}

void Game::handleMainMenuState()
{
    //menu buttons clicked events
    sf::Event event;
    while (window.pollEvent(event))
        handleGeneralInput(event);
    renderMenu();
    playMusic();
}

void Game::toGameOverState()
{
    gui.showHighScoreWidget(score);
    mainLoopCallback = std::bind(&Game::handleGameOverState, this);
}

void Game::toMainMenuState(bool restartMusic)
{
    gui.hideReturnMMConfirmWidget();
    gui.hidePauseMenuWidget();
    gui.hideHighScoreWidget();
    gui.showMainMenuWidget();
    if (restartMusic)
    {
        music.stop();
        if (!resMan.map(music, "day27.ogg")) exit(1);
    }
    mainLoopCallback = std::bind(&Game::handleMainMenuState, this);
}

void Game::toMMFromPS()
{
    gui.hidePauseMenuWidget();
    gameTimer.toggle();
    timeSinceLastMatch.toggle();
    mainLoopCallback = std::bind(&Game::handlePlayingState, this);
}

bool Game::loadHighScores()
{
    std::ifstream fin(HISCR_PATH);
    if (!fin) return 0;
    highScores.clear();
    highScores.resize(10);
    for (int i = 0; i < 10; ++i)
        if (!highScores[i].load(fin))
            break;
    return 1;
}

void Game::saveHighScores()const
{
    std::ofstream fout(HISCR_PATH);
    if (!fout) exit(1);
    for (auto const& hiScore : highScores)
        hiScore.save(fout);
}
