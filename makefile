# EXECUTABLES - DO NOT MODIFIED
CXX = g++
RM = rm -f
MKDIR = mkdir -p


# PROJECT NAME - will be output executable name
PROJECT = KawaiisClone


# Include
INCLS = -IH:/TGUI-0.7.2/include

# Linker
LIBS = -LH:/TGUI-0.7.2/lib


# BUILD TARGETS - change your compile flags here
BUILD_RELEASE = Release
CFLAGS_RELEASE = -O2
LFLAGS_RELEASE = -s

# Add another build target here

# Switch your build target here
BUILD = $(BUILD_RELEASE)
CFLAGS = -Wall $(CFLAGS_RELEASE)
LFLAGS = $(LFLAGS_RELEASE) -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lzip -lboost_system-mt -lboost_random-mt -ltgui -mwindows


# Input directory
SRCDIR = .

# Output directories - DO NOT MODIFIED
OBJDIR = build/$(BUILD)/obj
EXEDIR = build/$(BUILD)/bin
DEPDIR = build/.dependencies
EXE = $(EXEDIR)/$(PROJECT)
EXEWIN = build\$(BUILD)\bin\$(PROJECT).exe

SRCS = $(wildcard $(SRCDIR)/*.cpp)
HDRS = $(wildcard $(SRCDIR)/*.h)
DEPS = $(SRCS:$(SRCDIR)/%.cpp=$(DEPDIR)/%.d)
OBJS = $(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)

$(DEPDIR)/%.d: $(SRCDIR)/%.cpp | $(DEPDIR)
	$(CXX) $< -MM -MT "$(<:%.cpp=$(OBJDIR)/%.o)" > $@
	$(CXX) $< -MM -MT $@ >> $@

-include $(DEPS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJDIR)
	$(CXX) $(CFLAGS) $(INCLS) -c $< -o $@

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

$(DEPDIR):
	$(MKDIR) $(DEPDIR)
ifdef HIDE
	$(HIDE) $(DEPDIR)
endif

$(EXEDIR):
	$(MKDIR) $(EXEDIR)

run:
	start cmd /k "echo %time%  && $(EXEWIN) && echo. && pause && exit"

$(EXE): $(OBJS) | $(EXEDIR)
	$(CXX) $(LIBS) $(OBJS) $(LFLAGS) -o $(EXE)
	
build: $(EXE)
	

clean:
	$(RM) $(EXE)
	$(RM) $(OBJS)

.PHONY: run build clean
