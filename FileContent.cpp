#include "FileContent.h"

FileContent::FileContent() : data(NULL), size(0)
{
}

FileContent::~FileContent()
{
    delete [] data;
}

bool FileContent::loadToMemory(const char* name, const ZipPack& pack)
{
    if (data) return false; //prevent reload

    // Search for the file of given name
    struct zip_stat st;
    zip_stat_init(&st);
    zip_stat(pack.zPtr(), name, 0, &st);
    if (!(st.valid & ZIP_STAT_INDEX)) return false;

    // Alloc memory for its uncompressed contents
    size = st.size;
    data = new char[size];

    // Read the compressed file
    zip_file *f = zip_fopen_index(pack.zPtr(), st.index, 0);
    zip_fread(f, data, size);
    zip_fclose(f);
    return true;
}
