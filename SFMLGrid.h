#ifndef SFMLGRID_H
#define SFMLGRID_H

#include <SFML/Graphics.hpp>
#include "Grid.h"
#include "ResourceManager.h"

namespace kawaii
{
class SFMLGrid : public Grid, public sf::Drawable, public sf::Transformable
{
public:
    SFMLGrid(int, int, int);
    bool setDrawingInfo(int, int, ResourceManager&, const std::string&, int, int);
    void handleMouseClick(int, int);
    Coord toGridIndex(int, int)const;
    void setGridColors(const std::vector<sf::Color>&);
private:
    sf::IntRect getTexIntRect(int)const;
    virtual void draw(sf::RenderTarget&, sf::RenderStates)const;
    void drawGrid(sf::RenderTarget&, sf::RenderStates)const;
    void drawPath(sf::RenderTarget&, sf::RenderStates)const;
private:
    sf::Texture texture;
    int spriteW, spriteH; //sprite size
    int texR, texC; //texture's dimension
    int cellW, cellH; //base size

    sf::Color cellOutlineColor;
    sf::Color cellFillColor;
    sf::Color cellSelectedColor;
    sf::Color cellHintColor;
};
}

#endif // SFMLGRID_H
