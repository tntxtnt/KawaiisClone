#include "SplashScreen.h"

void SplashScreen::run(sf::RenderWindow& window, ResourceManager& resMan)
{
#ifndef NO_SPLASH_SCREEN
    // Get splash file
    if (!resMan.acquire("images", "splash.png")) exit(1);
    // Load an image with transparent parts that will define the shape of the window
    sf::Image splashImg;
    resMan.map(splashImg, "splash.png");

    auto splashSz = splashImg.getSize();
    window.create(sf::VideoMode(splashSz.x, splashSz.y, 32), "", sf::Style::None);

    auto desktopRect = sf::VideoMode::getDesktopMode();
    int windowPosX = (desktopRect.width  - splashImg.getSize().x) / 2;
    int windowPosY = (desktopRect.height - splashImg.getSize().y) / 2;
    window.setPosition(sf::Vector2i(windowPosX, windowPosY));
    window.setFramerateLimit(60);

    unsigned char opacity = 0;
    window.setVisible(false);
    #if defined (SFML_SYSTEM_WINDOWS)
    SetWindowLong(window.getSystemHandle(), GWL_EXSTYLE, // hide in taskbar
                  WS_EX_TOOLWINDOW);
    #endif
    setShape(window.getSystemHandle(), splashImg);
    setTransparency(window.getSystemHandle(), opacity);
    window.setVisible(true);

    // We will also draw the image on the window instead of just showing an empty window with the wanted shape
    sf::Texture splashTexture;
    sf::Sprite splashSprite;
    if (!splashTexture.loadFromImage(splashImg)) exit(1);
    splashSprite.setTexture(splashTexture);

    // Splash screen loop
    sf::Clock splashTimer;
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        if (opacity < 255)
        {
            if (opacity > 250) opacity = 255; else opacity += 5;
            setTransparency(window.getSystemHandle(), opacity);
        }
        if (splashTimer.getElapsedTime() > SPLASH_TIME)
            window.close();

        window.clear(sf::Color::Transparent);
        window.draw(splashSprite);
        window.display();
    }
#endif //DEBUG
}
