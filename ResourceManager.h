#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include "FileContent.h"
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <map>

using ResourceHolder = std::map<std::string,FileContent>;

class ResourceManager
{
public:
    ~ResourceManager();
    bool mount(const std::string&);
    void unmount();
    const std::string& getPackName()const { return packName; }
    bool acquire(const std::string&, const std::string&);
    void release(const std::string&, const std::string&);
    bool loadAll(); //load all at start then unmount
    bool map(sf::Image&, const std::string&);
    bool map(sf::Texture&, const std::string&);
    bool map(sf::SoundBuffer&, const std::string&);
    bool map(sf::Music&, const std::string&);
    bool map(sf::Font&, const std::string&);
private:
    std::string packName;
    ZipPack pack;
    std::map<std::string,ResourceHolder> resources;
};

#endif // RESOURCEMANAGER_H
