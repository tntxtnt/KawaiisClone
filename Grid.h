#ifndef GRID_H
#define GRID_H

#include <vector>
#include <algorithm>
#include <queue>
#include <random>
#ifndef _MSC_VER
#include <boost/random/random_device.hpp>
#endif
#include <chrono>
#include <functional>

namespace kawaii
{
struct Coord {
    int r, c;
    Coord(int r=-1, int c=-1) : r(r), c(c) {}
};

const unsigned H_DIR = 1;
const unsigned V_DIR = 2;
const unsigned B_DIR = 3;
struct PathfindingCell {
    unsigned direction;
    int turns;
    PathfindingCell(unsigned dir=B_DIR, int turns=-1)
    : direction(dir), turns(turns) {}
};

class Grid
{
public:
    using ShiftCallbackFunc = std::function<void(Grid&)> ;
    static const int EMPTY = 0;
    static void defaultShift(Grid&);
    static void shiftLeft(Grid&);
    static void shiftRight(Grid&);
    static void shiftUp(Grid&);
    static void shiftDown(Grid&);
    static void shiftOutVertical(Grid&);
    static void shiftOutHorizontal(Grid&);
    static void shiftInVertical(Grid&);
    static void shiftInHorizontal(Grid&);
    void setShiftCallback(ShiftCallbackFunc);
    void shift();
public:
    Grid(int w, int h, int pairs);
    void reset(int=0);
    void resize(int w, int h);
    int rows()const { return h; }
    int cols()const { return w; }
    const std::vector<int>& operator[](size_t i)const { return data[i]; }
    const std::pair<Coord,Coord>& getSelections()const { return selections; }
    bool hasTile(const Coord&)const;
    void shuffle();
    void select(int r, int c);
    void deselectLast();
    int  selected()const;
    bool match()const;
    void findPath();
    bool hasPath()const;
    void removePair();
    void findRandomMatch();
    bool hasHint()const { return hiddenHint.first.r != -1; }
    void showHint();
    bool cleared()const { return totalPairs == 0; }
    void swapTile(int, int, int, int);
protected:
    void clearState();
    std::vector<int> toGridRow()const;
    void parseToGrid(const std::vector<int>&);
    bool inRange(int, int)const;
    void checkDestination(int, int, int, std::string&, char);
    void explore(int, int, int, int, std::queue<Coord>&);
    void exploreTop(const Coord&, int, std::string&, std::queue<Coord>&);
    void exploreBottom(const Coord&, int, std::string&, std::queue<Coord>&);
    void exploreLeft(const Coord&, int, std::string&, std::queue<Coord>&);
    void exploreRight(const Coord&, int, std::string&, std::queue<Coord>&);
    void findConnectedPath();
    std::vector<Coord> buildPath(char);
    static Coord getDirVect(char);
    static int getPathLength(const std::vector<Coord>&);
private:
    std::mt19937 rng;
protected:
    std::vector<std::vector<int>> data;
    int w, h, groupSize, totalPairs;
    std::pair<Coord,Coord> selections;
    std::pair<Coord,Coord> hint, hiddenHint;
    std::vector<std::vector<PathfindingCell>> work;
    int r1, c1, r2, c2;
    std::vector<Coord> path;
    ShiftCallbackFunc shiftFunc;
};
}

#endif // GRID_H
