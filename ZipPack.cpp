#include "ZipPack.h"
#include <iostream>

ZipPack::ZipPack(const char* zipname) : z(NULL)
{
    open(zipname);
}

ZipPack::~ZipPack()
{
    if (z) zip_close(z);
}

bool ZipPack::open(const char* zipname)
{
    if (z || !zipname) return false; //prevent re-open
    int err = 0;
    z = zip_open(zipname, ZIP_RDONLY, &err);
    if (err) return false;
    return true;
}

void ZipPack::close()
{
    zip_close(z);
    z = NULL;
}

size_t ZipPack::fcount()const
{
    return zip_get_num_entries(z, 0);
}

std::vector<std::string> ZipPack::getAllFileNames()const
{
    std::vector<std::string> names;
    struct zip_stat st;
    zip_stat_init(&st);
    size_t count = fcount();
    for (size_t i = 0; i < count; ++i)
    {
        zip_stat_index(z, i, 0, &st);
        names.push_back(st.name);
    }
    return names;
}
