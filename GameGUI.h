#ifndef GAMEGUI_H
#define GAMEGUI_H

#include <TGUI/TGUI.hpp>
#include "GameHelperFunctions.h"

const sf::Time FADE_IN_DELAY = sf::milliseconds(500);

class GameGUI : public tgui::Gui
{
public:
    void loadMainMenuWidget();
    void hideMainMenuWidget();
    void showMainMenuWidget();

    void loadHighScoreWidget();
    void hideHighScoreWidget();
    void showHighScoreWidget(int);

    void loadPauseMenuWidget();
    void hidePauseMenuWidget();
    void showPauseMenuWidget();
    void hideReturnMMConfirmWidget();
    void showReturnMMConfirmWidget();
};


#endif // GAMEGUI_H

