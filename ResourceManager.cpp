#include "ResourceManager.h"

ResourceManager::~ResourceManager()
{
    unmount();
}

bool ResourceManager::mount(const std::string& zipname)
{
    if (pack) return false;
    packName = zipname;
    return pack.open(zipname.c_str());
}

void ResourceManager::unmount()
{
    if (pack)
    {
        pack.close();
        packName = "";
    }
}

bool ResourceManager::acquire(const std::string& category,
                              const std::string& path)
{
    if (!pack) return false;
    auto filePtr = resources[category].find(path);
    if (filePtr != resources[category].end()) return true; //already acquired
    std::string fullPath = category + "/" + path;
    return resources[category][path].loadToMemory(fullPath.c_str(), pack);
}

void ResourceManager::release(const std::string& category,
                              const std::string& path)
{
    auto catPtr = resources.find(category);
    if (catPtr == resources.end()) return; //no such category
    auto filePtr = catPtr->second.find(path);
    if (filePtr == catPtr->second.end()) return; //no such file in this cat
    catPtr->second.erase(filePtr);
}

bool ResourceManager::loadAll()
{
    for (auto fpath : pack.getAllFileNames())
    {
        int sid = fpath.find('/');
        std::string category = fpath.substr(0, sid);
        std::string fname = fpath.substr(sid+1);
        if (fname.empty()) continue;
        acquire(category, fname);
    }
    return 1;
}

bool ResourceManager::map(sf::Image& t, const std::string& id)
{
    ResourceHolder& h = resources["images"];
    if (h.find(id) == h.end() && !acquire("images", id)) return 0;
    return t.loadFromMemory(h[id].getData(), h[id].getSize());
}

bool ResourceManager::map(sf::Texture& t, const std::string& id)
{
    ResourceHolder& h = resources["textures"];
    if (h.find(id) == h.end() && !acquire("textures", id)) return 0;
    return t.loadFromMemory(h[id].getData(), h[id].getSize());
}

bool ResourceManager::map(sf::SoundBuffer& t, const std::string& id)
{
    ResourceHolder& h = resources["sounds"];
    if (h.find(id) == h.end() && !acquire("sounds", id)) return 0;
    return t.loadFromMemory(h[id].getData(), h[id].getSize());
}

bool ResourceManager::map(sf::Music& t, const std::string& id)
{
    ResourceHolder& h = resources["musics"];
    if (h.find(id) == h.end() && !acquire("musics", id)) return 0;
    return t.openFromMemory(h[id].getData(), h[id].getSize());
}

bool ResourceManager::map(sf::Font& t, const std::string& id)
{
    ResourceHolder& h = resources["fonts"];
    if (h.find(id) == h.end() && !acquire("fonts", id)) return 0;
    return t.loadFromMemory(h[id].getData(), h[id].getSize());
}
