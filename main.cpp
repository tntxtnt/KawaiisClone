#include "Game.h"

const std::string RES_PATH   = "assets/assets.zip";
const std::string HISCR_PATH = "highscores.dat";

const int WINDOW_W   = 800;
const int WINDOW_H   = 600;
const int CELL_W     =  40;
const int CELL_H     =  50;
const int CELL_ROWS  =  9;
const int CELL_COLS  = 16;  //CELL_ROWS*CELL_COLS must divided by 4
const int GRID_POS_X = (WINDOW_W - CELL_COLS * CELL_W)/2;
const int GRID_POS_Y = (WINDOW_H - CELL_ROWS * CELL_H)/2 + 20;
const int FPS = 30;

const sf::Time MATCH_DELAY      = sf::seconds(0.5f);
const sf::Time NEXT_LEVEL_DELAY = sf::seconds(3.0f);

const sf::Time SPLASH_TIME = sf::seconds(4.0f);
const sf::Time MATCH_TIME = sf::seconds(450.0f);
const sf::Time HINT_TIME = sf::seconds(30.0f);

const int MAX_LEVEL = 9;
const int SCORE_PER_MATCH = 20; //20
const int SCORE_PER_SHUFF = 50; //50
const int SCORE_PER_HINT  = 10; //10
const int SCORE_PER_SEC   =  1; //1

const int INIT_HINT_COUNT = 10;
const int INIT_SHUF_COUNT = 10;
const int ADDED_HINT_COUNT = 5;
const int ADDED_SHUF_COUNT = 1;

const int MAX_PLAYER_NAME_LEN = 20;
const int SAVE_NAME_LEN = 6*MAX_PLAYER_NAME_LEN+1;

int main()
{
    Game::instance().run();
}
