#ifndef FILECONTENT_H
#define FILECONTENT_H

#include <cstdio>
#include "ZipPack.h"

class FileContent
{
public:
    FileContent();
    ~FileContent();
    bool loadToMemory(const char*, const ZipPack&);
    char*  getData()const { return data; }
    size_t getSize()const { return size; }
private:
    FileContent(const FileContent&); //copy ctor
    FileContent& operator=(const FileContent&); //assignment operator
private:
    char* data;
    size_t size;
};

#endif // FILECONTENT_H
