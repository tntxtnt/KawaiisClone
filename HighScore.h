#ifndef HIGHSCORE_H
#define HIGHSCORE_H

#include <SFML/Graphics.hpp>
#include <fstream>
#include <cstring>
#include "GameHelperFunctions.h"
#ifdef _MSC_VER
#include <iterator>
#endif

extern const int MAX_PLAYER_NAME_LEN;
extern const int SAVE_NAME_LEN;

struct HighScore
{
    sf::String name;
    int score;

    HighScore(int scr=0) : name(), score(scr) {}
    bool load(std::ifstream&);
    void save(std::ofstream&)const;
};

#endif // HIGHSCORE_H
