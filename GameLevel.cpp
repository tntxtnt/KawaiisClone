#include "GameLevel.h"

GameLevel::GameLevel(kawaii::SFMLGrid& grid, sf::Music& musicPlayer,
                     sf::Sprite& bgSprite, ResourceManager& resMan)
: shiftFuncs(MAX_LEVEL, kawaii::Grid::defaultShift),
  musicIds{"day24.ogg", "day27.ogg", "day53.ogg", "day57.ogg", "day69.ogg"},
  bgTexs(MAX_LEVEL),
  gridColors{
      { {  0, 64,128}, {180,227,239}, { 43,168,200}, {127,255, 64} },//BLUE
      { {155, 57,108}, {237,177,237}, {221,108,221}, {127,255, 64} },//PURPLE
      { {183,171, 21}, {255,255,215}, {176,176,100}, {127,255, 64} },//YELLOW
      { { 62,142, 66}, {206,255,208}, {136,221,140}, {127,255, 64} },//GREEN
      { {155, 57,108}, {237,177,237}, {221,108,221}, {127,255, 64} },//PURPLE
      { {255,128, 64}, {255,198,140}, {210,147, 98}, {127,255, 64} },//ORANGE
      { {163, 48, 48}, {255,213,213}, {176,113,113}, {127,255, 64} },//RED
      { {  0, 64,128}, {180,227,239}, { 43,168,200}, {127,255, 64} },//BLUE
      { {183,171, 21}, {255,255,215}, {176,176,100}, {127,255, 64} } //YELLOW
  },
  grid(grid), musicPlayer(musicPlayer), bgSprite(bgSprite), resMan(resMan)
{
    // Register shift functions
    shiftFuncs[1] = kawaii::Grid::shiftUp;
    shiftFuncs[2] = kawaii::Grid::shiftDown;
    shiftFuncs[3] = kawaii::Grid::shiftLeft;
    shiftFuncs[4] = kawaii::Grid::shiftRight;
    shiftFuncs[5] = kawaii::Grid::shiftOutVertical;
    shiftFuncs[6] = kawaii::Grid::shiftOutHorizontal;
    shiftFuncs[7] = kawaii::Grid::shiftInVertical;
    shiftFuncs[8] = kawaii::Grid::shiftInHorizontal;
}

void GameLevel::sync(int lvl)
{
    // Bind mainLoopHandler
    grid.setShiftCallback(shiftFuncs[lvl]);
    // Bind music
    musicPlayer.stop();
    if (!resMan.map(musicPlayer, musicIds[lvl % musicIds.size()]))
        exit(1);
    // Bind background image
    bgSprite.setTexture(bgTexs[lvl]);
    // Bind grid colors
    grid.setGridColors(gridColors[lvl]);
}
