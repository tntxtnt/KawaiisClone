# Kawaii's Clone
Find matches tiles that are connected by at most 3 lines.  
- F1 to show hint   
- F5 to shuffle  
- Right click to deselect  
- 7.5 min per game  

### Screenshots

![play](screenshots/play.png)

### Resource's requirements
- assets/assets.zip (textures, images, fonts, sounds, and musics)

### Dependencies
- SFML v2.4.1 (zlib/png license)  
- libzip v1.1.3 (Libzip license)  
- Boost.System (Boost license)  
- Boost.Random (Boost license)  
- TGUI 0.7.2 (TGUI license)  

### Other licenses
- Falling Sky font by Cannot Into Space Fonts (SIL Open Font License)  

### To Do List

### Wish list
- New save file (sqlite, json, ...?)  
- Customize main menu GUI  
- Customize high score GUI  

### Documentation
- FSM diagram using http://madebyevan.com/fsm/  
