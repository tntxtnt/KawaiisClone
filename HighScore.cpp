#include "HighScore.h"

bool HighScore::load(std::ifstream& fin)
{
    std::vector<char> utf8Name(SAVE_NAME_LEN, '\0');
    if (!fin.read(&utf8Name[0], SAVE_NAME_LEN))
        return 0;
    char utf8Score[6];
    if (!fin.read(utf8Score, 6))
        return 0;
    this->name = sf::String::fromUtf8(&utf8Name[0],
                                      &utf8Name[0]+strlen(&utf8Name[0]));
    this->score = std::stoi(utf8Score);
    return 1;
}

void HighScore::save(std::ofstream& fout)const
{
    auto utf8Name = this->name.toUtf8();
    for (auto c : utf8Name)
        fout.put(c);
    for (int i = utf8Name.size(); i < SAVE_NAME_LEN; ++i)
        fout.put('\0');
    for (auto c : rightJustify(this->score, 5))
        fout.put(c);
    fout.put('\0');
}
