#ifndef GAMELEVEL_H
#define GAMELEVEL_H

#include "SFMLGrid.h"
#include "ResourceManager.h"

extern const int MAX_LEVEL;

struct GameLevel
{
    std::vector<kawaii::Grid::ShiftCallbackFunc> shiftFuncs;
    std::vector<std::string> musicIds;
    std::vector<sf::Texture> bgTexs;
    std::vector<std::vector<sf::Color>> gridColors;
    kawaii::SFMLGrid& grid;
    sf::Music& musicPlayer;
    sf::Sprite& bgSprite;
    ResourceManager& resMan;

    GameLevel(kawaii::SFMLGrid&, sf::Music&, sf::Sprite&, ResourceManager&);
    void sync(int);
};

#endif // GAMELEVEL_H
