//https://github.com/texus/TransparentWindows
//http://en.sfml-dev.org/forums/index.php?topic=18337.0

#include <SFML/Graphics.hpp>
#if defined (SFML_SYSTEM_WINDOWS)
#include <windows.h>
bool setShape(HWND hWnd, const sf::Image& image);
bool setTransparency(HWND hWnd, unsigned char alpha);
#elif defined (SFML_SYSTEM_LINUX)
#include <X11/Xatom.h>
#include <X11/extensions/shape.h>
bool setShape(Window wnd, const sf::Image& image);
bool setTransparency(Window wnd, unsigned char alpha);
#undef None // None conflicts with SFML
#elif defined (SFML_SYSTEM_MACOS)
bool setShape(sf::WindowHandle handle, const sf::Image& image);
bool setTransparency(sf::WindowHandle handle, unsigned char alpha);
#else
bool setShape(sf::WindowHandle handle, const sf::Image& image);
bool setTransparency(sf::WindowHandle handle, unsigned char alpha);
#endif
