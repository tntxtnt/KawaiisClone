#include "GameHUD.h"

void GameHUD::setup()
{
    // Setup timer bar sprite
    timerBarSprite.setTexture(timerBarTex);
    timerBarSprite.setPosition((WINDOW_W - timerBarTex.getSize().x)/2,
                               (95 - timerBarTex.getSize().y)/2);
    // Create time elapsed bar
    timeElapsedBar.setSize(sf::Vector2f(0, 15));//timerBar's height
    timeElapsedBar.setFillColor(sf::Color::Black);//background color
    timeElapsedBar.setPosition(WINDOW_W - timerBarSprite.getPosition().x - 2,
        timerBarSprite.getPosition().y + timerBarTex.getSize().y - 2);//outline=2
    timeElapsedBar.setRotation(180);
    // Setup scoreText
    scoreTextPosX = WINDOW_W - 80 - CELL_W/2;
    scoreText.setCharacterSize(40);
    scoreText.setFillColor(sf::Color::White);
    // Setup shuffleSprite
    shuffleSprite.setTexture(shuffleTex);
    shuffleSprite.setPosition(135, 32);
    // Setup shuffleSprite
    hintSprite.setTexture(hintTex);
    hintSprite.setPosition(30, 32);
    // Setup shuffleCountText
    shuffleCountText.setCharacterSize(36);
    shuffleCountText.setPosition(shuffleSprite.getPosition() + sf::Vector2f(35, -5));
    shuffleCountText.setFillColor(sf::Color::White);
    // Setup hintCountText
    hintCountText.setCharacterSize(36);
    hintCountText.setPosition(hintSprite.getPosition() + sf::Vector2f(35, -5));
    hintCountText.setFillColor(sf::Color::White);
    // Set hoverRect
    hoverRect.setSize(sf::Vector2f(CELL_W-5,CELL_H-5));
    hoverRect.setFillColor(sf::Color::Transparent);
    hoverRect.setOutlineThickness(2);
    hoverRect.setOutlineColor(sf::Color::Red);
}

void GameHUD::updateScoreText(int score)
{
    std::string scoreStr = std::to_string(score);
    scoreText.setString(scoreStr);
    if (scoreStr.size() < 4)
        scoreText.setPosition(scoreTextPosX, 20);
    else
        scoreText.setPosition(scoreTextPosX - 20*(scoreStr.size()-3), 20);
}

void GameHUD::updateShuffleCountText(int shuffleCount)
{
    shuffleCountText.setString(std::to_string(shuffleCount));
}

void GameHUD::updateHintCountText(int hintCount)
{
    hintCountText.setString(std::to_string(hintCount));
}

void GameHUD::syncHoverRectPosition()
{
    hoverRect.setPosition(hoverCoord.c*CELL_W + grid.getPosition().x + 2,
                          hoverCoord.r*CELL_H + grid.getPosition().y + 2);
}

void GameHUD::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
    states.transform *= getTransform();

    target.draw(timerBarSprite, states);
    target.draw(timeElapsedBar, states);
    target.draw(scoreText, states);
    target.draw(shuffleCountText, states);
    target.draw(shuffleSprite, states);
    target.draw(hintCountText, states);
    target.draw(hintSprite, states);
    if (hoverCoord.r != -1 && grid.hasTile(hoverCoord))
        target.draw(hoverRect, states);
}
